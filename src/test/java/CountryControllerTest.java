import edu.spring.Main;
import edu.spring.domain.Country;
import edu.spring.dto.CountryDto;
import edu.spring.dto.CountryListTransfer;
import edu.spring.repostory.CountryRepository;
import edu.spring.rest.NotFoundException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.ContextConfiguration;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = Main.class)
public class CountryControllerTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private CountryRepository repository;

    @BeforeEach
    public void before() {
        repository.save(new Country(1, "Russia", "ru"));
        repository.save(new Country(2, "Japan", "jp"));
        repository.save(new Country(3, "German", "de"));
    }

    @AfterEach
    public void after() {
        repository.deleteAll();
    }

    @Test
    public void getAllCountries() {
        CountryListTransfer countries = restTemplate.getForObject(
                getHost() + "/country/all", CountryListTransfer.class);

        assertEquals(3, countries.getCountries().size());
    }

    @Test
    public void getCountryById() {
        CountryDto country = restTemplate.getForObject(getHost() + "/country/1", CountryDto.class);

        assertEquals("ru", country.getCodeName());
    }

    @Test
    public void createCountry() {
        CountryDto country = new CountryDto(4, "USA", "en_Us");

        CountryDto created = restTemplate.postForObject(getHost() + "/country", country, CountryDto.class);

        assertEquals(country.getId(), created.getId());
    }

    @Test
    public void updateCountry() {
        restTemplate.exchange(
                getHost() + "country/1/update?name=Russian Federation", HttpMethod.PUT, HttpEntity.EMPTY, Void.class);

        Country updatedCountry = repository.findById(1).orElseThrow(NotFoundException::new);

        assertEquals("Russian Federation", updatedCountry.getName());
    }

    @Test
    public void deleteCountry() {
        restTemplate.exchange(getHost() + "/country/3", HttpMethod.DELETE, HttpEntity.EMPTY, Void.TYPE);

        Optional<Country> countryOpt = repository.findById(3);

        assertFalse(countryOpt.isPresent());
    }

    private String getHost() {
        return "http://localhost:" + port;
    }
}
