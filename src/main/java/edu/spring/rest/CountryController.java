package edu.spring.rest;

import edu.spring.domain.Country;
import edu.spring.dto.CountryDto;
import edu.spring.dto.CountryListTransfer;
import edu.spring.repostory.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class CountryController {

    @Autowired
    private CountryRepository countryRepository;

    @GetMapping("/country/all")
    @ResponseBody
    public CountryListTransfer getAll() {
        return new CountryListTransfer(
                countryRepository.findAll().stream()
                        .map(CountryDto::toDto)
                        .collect(Collectors.toList())
        );
    }

    @GetMapping("/country/{id}")
    public CountryDto findById(@PathVariable("id") Integer id) {
        Country country = countryRepository.findById(id).orElseThrow(NotFoundException::new);
        return CountryDto.toDto(country);
    }

    @PostMapping("/country")
    public CountryDto create(@RequestBody CountryDto countryDto) {
        Country country = countryRepository.save(CountryDto.toDomain(countryDto));
        return CountryDto.toDto(country);
    }

    @PutMapping("/country/{id}/update")
    public void update(@PathVariable("id") Integer id, @RequestParam String name) {
        Country country = countryRepository.findById(id).orElseThrow(NotFoundException::new);
        country.setName(name);
    }

    @DeleteMapping("/country/{id}")
    public void delete(@PathVariable("id") Integer id) {
        countryRepository.deleteById(id);
    }
}
