package edu.spring.dto;

import edu.spring.domain.Country;

public class CountryDto {

    private Integer id;

    private String name;

    private String codeName;

    public CountryDto() {
    }
    public CountryDto(Integer id, String name, String codeName) {
        this.id = id;
        this.name = name;
        this.codeName = codeName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCodeName() {
        return codeName;
    }

    public void setCodeName(String codeName) {
        this.codeName = codeName;
    }

    public static Country toDomain(CountryDto dto) {
        return new Country(dto.getId(), dto.getName(), dto.getCodeName());
    }

    public static CountryDto toDto(Country country) {
        return new CountryDto(country.getId(), country.getName(), country.getCodeName());
    }
}
