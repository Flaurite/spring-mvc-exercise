package edu.spring.dto;

import java.util.List;

public class CountryListTransfer {

    private List<CountryDto> countries;

    public CountryListTransfer() {
    }

    public CountryListTransfer(List<CountryDto> countries) {
        this.countries = countries;
    }

    public List<CountryDto> getCountries() {
        return countries;
    }

    public void setCountries(List<CountryDto> countries) {
        this.countries = countries;
    }
}
